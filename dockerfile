FROM nginx

# Install required packages
RUN apt-get update && apt-get install -y \
    package1 \
    package2 \
    # Add any additional packages if needed
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/share/nginx/html

# Create a file with "hello world" content
RUN echo "hello world" > sim.html

# Start Nginx server
CMD ["nginx", "-g", "daemon off;"]
